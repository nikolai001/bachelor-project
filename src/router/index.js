import { createRouter, createWebHashHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/login',
    name: 'login',
    component: () => import(/* webpackChunkName: "login" */ '../views/LoginView.vue')
  },
  {
    path: '/register',
    name: 'register',
    component: () => import(/* webpackChunkName: "register" */ '../views/RegisterView.vue')
  },
  {
    path: '/profile',
    name: 'profile',
    component: () => import(/* webpackChunkName: "profile" */ '../views/ProfileView.vue')
  },
  {
    path: '/information',
    name: 'information',
    component: () => import(/* webpackChunkName: "information" */ '../views/InformationView.vue')
  },
  {
    path: '/scenarios',
    name: 'scenarios',
    component: () => import(/* webpackChunkName: "scenarios overview" */ '../views/ScenariosOverviewView.vue')
  },
  {
    path: '/scenario-operations',
    name: 'scenario-operations',
    component: () => import(/* webpackChunkName: "scenarios overview" */ '../views/ScenarioCRUDview.vue')
  },
  {
    path: '/scenario/:id',
    name: 'scenario',
    component: () => import(/* webpackChunkName: "scenarios overview" */ '../views/ScenarioView.vue')
  }
]

const router = createRouter({
  history: createWebHashHistory(process.env.BASE_URL),
  routes
})

export default router
