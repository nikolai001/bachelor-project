import { defineStore } from 'pinia'
import { useStorage } from '@vueuse/core'

export const useUserStore = defineStore('user', {
  state: () => ({ // Building block for the user
    user:useStorage('user',({
      id: '',
      jwt: '',
      profilePic: '',
    }))
  }),

  getters: { // Gets the user from localstore, gonna block user actions if the local user trying to access user information isn't in fact that user
    getUser() {
      return this.user
    }
  },

  actions: {
    updateUserDetails(user) { // Updates the user in localstore, only to be used while logging in (Maybe on registration)
      this.user.id = user.user.id
      this.user.jwt = user.jwt
      if (user.user.profilePic === null) {
        this.user.profilePic = require('../assets/no_image.jpg')
        return
      }
      this.user.profilePic = process.env.VUE_APP_ROOT_API + user.user.profilePic.url
    },

    clearUserDetails () {
      this.user = {}
    }
  }
})

export const useCurrentScenarioStore = defineStore('currentScenario', {
  state: () => ({
    currentScenario:useStorage('currentScenario', ({ // Building block for current scenario
      id: '',
      title: '',
      content: '',
      tags: {}
    }))
  }),

  getters: { // Returns the scenario stored in localstore, will allow link sharing
    getCurrentScenario () {
      return this.currentScenario
    }
  },

  actions: { // Takes an object, the object sent contains a lot of unnecessary data so I have to update each property manually
    updateCurrentScenario (scenario) {
      this.currentScenario.id = scenario.id,
      this.currentScenario.title = scenario.title,
      this.currentScenario.content = scenario.content,
      this.currentScenario.tags = scenario.tags
    }
  }
})

export const useScenariosStore = defineStore('scenarios', {
  state: () => ({ // The state of scenarios, or the building block so to say
    scenarios:useStorage('scenarios', ({
      list: {}
    }))
  }),

  getters: { // Will list all scenarios in localstore
    getAllScenarios () {
      return this.scenarios
    }
  },

  actions: {
    populateScenarios (list) { // Accepts an array of scenarios
      this.scenarios.list = list
    },
  }
})

export const useSubPathStore = defineStore('subPath', {
  state: () => ({
    subPath:useStorage('subPath', ({
      path: ''
    }))
  }),

  getters: {
    getSubPath () {
      return this.subPath.path
    }
  },

  actions: {
    updateSubPath (path) {
      this.subPath.path = path
    }
  }
})

export const useSearchStore = defineStore('searchString', {
  state: () => ({
    searchString:useStorage('searchString', ({
      searchToken: ''
    }))
  }),

  getters: {
    getSearchToken () {
      return this.searchString.searchToken
    }
  },

  actions: {
    updateSearchToken (token) {
      this.searchString.searchToken = token = token.replace(/\s+/g, '+');
    },

    clearSearch () {
      this.searchString.searchToken = ''
    }
  }
})